# Blind-Fold Images
This is a simple python script taking in images from given path \
and creates an image that alternates between them vertically.

You can then create a blindfold matching the size the images were split to, \
allowing you to move it along the created image and only seeing on of the original images at a time.
