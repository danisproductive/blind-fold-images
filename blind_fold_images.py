from PIL import Image
import os
import argparse

IMAGES_PATH_ENV = 'imagePath'
SPLIT_COUNT_ENV = 'splitCount'
DEFAULT_IMAGES_PATH = './images/'
DEFAULT_TARGET_IMAGE_PATH = './images/result.jpg'
DEFAULT_SPLIT_COUNT = 40


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--imagesPath', help='Location of images to split', required=False)
    parser.add_argument('--splitCount', help='Amount of image switches to create (between all images)', required=False)
    parser.add_argument('--targetImagePath', help='Where to place the result image', required=False)

    return parser


def get_blind_fold_arguments(cli_parser):
    args = cli_parser.parse_args()
    images_path = args.imagesPath or DEFAULT_IMAGES_PATH
    target_images_path = args.targetImagePath or DEFAULT_TARGET_IMAGE_PATH
    split_count = int(args.splitCount) or DEFAULT_SPLIT_COUNT

    if images_path[-1] != '/':
        images_path += '/'

    return images_path, target_images_path, split_count


def get_source_images(images_path):
    images = []

    for imageName in os.listdir(images_path):
        if ".jpg" in imageName:
            image = Image.open(images_path + imageName)
            images.append(image)

    return images


def create_base_image(width, height):
    return Image.new("RGB", (width, height))


def place_image_parts(base_image, images, split_count, width, height):
    part_height = int(height / int(split_count))
    top = 0
    bottom = part_height

    for part in range(1, split_count + 1):
        for image in images:
            image_part = image.crop((0, top, width, bottom))
            base_image.paste(image_part, (0, top, width, bottom))
            top += part_height
            bottom += part_height


def create_blind_fold_image(images_path, split_count):
    images = get_source_images(images_path)
    if len(images) > 0:
        width, height = images[0].size
        blind_fold_image = create_base_image(width, height)
        place_image_parts(blind_fold_image, images, split_count, width, height)

        return blind_fold_image
    else:
        raise EnvironmentError("No images found in given path")


def main():
    cli_parser = create_parser()
    images_path, target_images_path, split_count = get_blind_fold_arguments(cli_parser)
    blind_fold_image = create_blind_fold_image(images_path, split_count)
    blind_fold_image.save(target_images_path)


if __name__ == "__main__":
    main()
